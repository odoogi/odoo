import json
import xmlrpc.client


#odoo url,db and credentials
url = "https://edu-1800057.odoo.com"
db = "edu-1800057"
username = "fernando.miron@gmail.com"
password = "odoo2018"

#open file and load into memory
with open("export.json") as json_file:
    json_data = json.load(json_file)

    #print the whole date
    #print(json_data)

    #print just first record, based on it's Odoo ID
    # print(json_data['10.0'])

#print the id and the qty and save result to res
    for key, value in json_data.items():
        print(key, value[1])
        res = (key, value[1])

prod_id = input("prod id please: ")

common = xmlrpc.client.ServerProxy('{}/xmlrpc/common'.format(url))
uid = common.authenticate(db, username, password, {})

models = xmlrpc.client.ServerProxy('{}/xmlrpc/object'.format(url))
models.execute_kw(db, uid, password,
                  'product.product', 'check_access_rights',
                  ['read'], {'raise_exception': False})

result = models.execute_kw(db, uid, password, 'product.product', 'read', [prod_id], {
    'fields': ['id', 'default_code', 'active', 'product_tmpl_id', 'barcode', 'volume', 'weight', 'message_last_post',
               'activity_date_deadline', 'create_uid', 'create_date', 'write_uid', 'write_date']})

print(json.dumps(result, indent=4))
