import xlrd
import xlwt
import simplejson as json
import sys
import odoo_import
import odoo_export


if (len(sys.argv) > 2):
    if (sys.argv[1] == "export"):
        odoo_export.odoo_export(sys.argv[2])
    elif (sys.argv[1] == "import"):
        odoo_import.odoo_import(sys.argv[2])
else:
    print("usage: py odoo.py export source_file.xlsx\n       py odoo.py import dest_file.xlsx")
