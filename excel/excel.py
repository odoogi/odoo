import xlrd
import xlwt
import simplejson as json
import sys
import simplejson as json
from xlutils.copy import copy


def excel2json(excel_name,json_name):
    # Open the xlsx file
    workbook = xlrd.open_workbook(excel_name, on_demand = True)
    # Retrieve the first sheet
    worksheet = workbook.sheet_by_index(0)
    dictionary = dict()
    # Add to dictionary the tuples needed to be updated
    for i in range (2, worksheet.nrows):
        if(worksheet.cell(i,0).value == 'MP'):      
             if (worksheet.cell(i, 1).value != ''):
                dictionary[worksheet.cell(i, 1).value] = \
                (
                        # Name of the product
                        worksheet.cell(i, 2).value, 
                        # Real quantity used
                        worksheet.cell(i, 8).value,
                        # Real cost of MP
                        worksheet.cell(i, 9).value
                        )
    # Open a file in write_mode
    with open(json_name, "w") as write_file:
        json.dump(dictionary, write_file)



def json2excel(json_name, excel_name):
    with open(json_name, "r") as read_file:
        json_imported = json.load(read_file)
    # Init workbook
    rb = xlrd.open_workbook(excel_name)
    rs = rb.sheet_by_index(0)
    wb = copy(rb)
    ws = wb.get_sheet(0)
    # Init worksheet
    dictionary = dict()
    for majorkey in json_imported.items():
        dictionary[majorkey[0]] = majorkey[1]
    i = 0
    for item in dictionary:
       # ws.write(i, 0, item)
        j = 7
        k = 0 
        # Looking in the tuple to put them in the excel
        for datas in dictionary[item]:
            if(k > 0):
                ws.write(i + 2, j, dictionary[item][k])
            j+=1
            k+=1
        i+=1
    wb.save(file_name)

excel2json("odoo.xlsx","odoo.json")

json2excel("odoo.json","1.xlsx")