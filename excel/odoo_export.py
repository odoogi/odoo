import xlrd
import simplejson as json

def odoo_export(file_name):
    # Open the xlsx file
    workbook = xlrd.open_workbook(file_name, on_demand = True)
    # Retrieve the first sheet
    worksheet = workbook.sheet_by_index(0)
    dictionary = dict()
    # Add to dictionary the tuples needed to be updated
    for i in range (2, worksheet.nrows):
        if(worksheet.cell(i,0).value == 'MP'):      
             if (worksheet.cell(i, 1).value != ''):
                dictionary[worksheet.cell(i, 1).value] = \
                (
                        # Name of the product
                        worksheet.cell(i, 2).value, 
                        # Real quantity used
                        worksheet.cell(i, 8).value,
                        # Real cost of MP
                        worksheet.cell(i, 9).value
                        )
    # Open a file in write_mode
    with open("export.json", "w") as write_file:
        json.dump(dictionary, write_file)
