First install modules:

pip install simplejson
pip install xlwt
pip install xlrd

How to use it:

py odoo.py export source.xlsx
py odoo.py import destination.xlsx
