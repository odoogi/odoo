import simplejson as json
import xlwt
import xlrd
from xlutils.copy import copy

def odoo_import(file_name):
    with open("import.json", "r") as read_file:
        json_imported = json.load(read_file)
    # Init workbook
    rb = xlrd.open_workbook('official.xlsx')
    rs = rb.sheet_by_index(0)
    wb = copy(rb)
    ws = wb.get_sheet(0)
    # Init worksheet
    dictionary = dict()
    for majorkey in json_imported.items():
        dictionary[majorkey[0]] = majorkey[1]
    i = 0
    for item in dictionary:
       # ws.write(i, 0, item)
        j = 7
        k = 0 
        # Looking in the tuple to put them in the excel
        for datas in dictionary[item]:
            if(k > 0):
                ws.write(i + 2, j, dictionary[item][k])
            j+=1
            k+=1
        i+=1
    wb.save(file_name)
