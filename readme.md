#Odoo
API ODOO

MISE A JOUR des COUTS REELS des M.P. dans la Nomenclature ODOO à partir des ORDRES DE FABRICATION REELS (Excel)


Icc Prof: (Alain Loyens) 2018-2018

Students:
```
BAYOKA MALOLO LUKUNGA
KAPIA BEYA
Antonio	MOMBAKA
Belmin	VEHAPI
Christopher R. P.	VAN DUFFEL
Dieudonne	TWAHIRWA
Djo J.	SIMPI MABIDI
Elias	EL HASSANI
Fernando	DE SOUZA MIRON
Hamza	ISLANE
Jean-Pierre	MUGISHA
Joseph	GOKALP
Kalumbo	NYAMABO MALELABO
Kassiri M	KROUBA
Khalid	MARCOUCH
Leila	TANOUTI
Mohamed	MAJOUT
Pascal	BANZIRA
Robinson E	COELLO SANCHEZ
Salix	NIBIGIRA
```



Use:

python login.py


More info & ref:

http://effbot.org/tkinterbook/tkinter-index.htm#class-reference

https://www.tutorialspoint.com/python/tk_messagebox.htm

placeholder
https://stackoverflow.com/questions/27820178/how-to-add-placeholder-to-an-entry-in-tkinter

<table>
https://tkdocs.com/tutorial/tree.html

compatible python 2 & 3
http://python-future.org/compatible_idioms.html


ODOO
https://useopenerp.com/v8/model/mail-thread

backup database
pg_dump -U postgres -W -h localhost shop > shop.sql

EXCEL 
https://automatetheboringstuff.com/chapter12/



Please send your tel number to 0474 624707

# Commandes git à faire:
* git branch : lister toutes les branches locales dans le dépôt courant
* git checkout [nom-de-branche] : Basculer sur la branche spécifiée et met à jour le répertoire de travail
* git branch [nom-de-branche]: Crée une nouvelle branche
* git add --all : faire un stage ou ajouter les fichiers 
* git commit -m "[message descreptif]": Enregistre des instantanés de fichiers 
* git status : Liste tous les nouveaux fichiers et les fichiers modifiés à commiter
* git branch --track [nom-de-branche] origin/[nom-de-branche] : récuperer une branche non existante sur votre dépôt local
* git log --oneline : lister tous les commits d'une branche avec leurs id


